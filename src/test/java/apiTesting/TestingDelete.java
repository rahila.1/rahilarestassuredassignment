package apiTesting;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestingDelete {
	
	@Test
	public void deleteOperations() {
		baseURI="https://reqres.in/";
		given().when().delete("api/users/2").then().statusCode(204);
		
	}

}
