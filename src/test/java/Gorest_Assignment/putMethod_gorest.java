package Gorest_Assignment;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;



public class putMethod_gorest {
	@Test
	public void puthMethod() {
		
		JSONObject request = new JSONObject();
		  request.put("name", "Rahi123");
		  request.put("email", "rahila1929@gmail.com");
		  request.put("gender", "Female");
		  request.put("status", "active");
		  
		  
		  baseURI="https://gorest.co.in";
		  given().header("Authorization","Bearer e842b0fe20b3f4193a6e90e8fe9561f09e5172813b3fc8ab970b2aa51ca618b6")
		  .contentType("application/json")
		  .body(request.toJSONString()).when().put("/public/v2/users/194128")
		  .then().statusCode(200).log().all();

	}

}
