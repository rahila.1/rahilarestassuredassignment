package Gorest_Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class postMethod_gorest {
	@Test(priority = 1)
	public void postMethods() {
		 JSONObject request = new JSONObject();
		  request.put("name", "Rahilarahi");
		  request.put("email", "rahi1904@gmail.com");
		  request.put("gender", "Female");
		  request.put("status", "active");
		  
		  
		  baseURI="https://gorest.co.in";
		  given().header("Authorization","Bearer e842b0fe20b3f4193a6e90e8fe9561f09e5172813b3fc8ab970b2aa51ca618b6")
		  .contentType("application/json")
		  .body(request.toJSONString()).when().post("/public/v2/users")
		  .then().statusCode(201).log().all();

	
	}
}
